import os
import gi
import gui
from pprint import pprint
from math import sqrt
from time import sleep
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk

def run():
    win = gui.MyWindow()
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()

if __name__ == "__main__":
    run()
