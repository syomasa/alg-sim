import gi
import enum
from math import sqrt
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib

class BoardObjects(enum.Enum):
    WALL = -1
    VISITED = 2
    DRAWN = 3
    PATH = 4

class PriorityQueue:
    # This will be naive implementation to make things
    # run smooher projects point is only to simulate
    # different kinds of algorithms
    def __init__(self):
        self.queue = []

    def p_insert(self, node):
        if not self.queue:
            self.queue.append(node)

        else:
            for i, element in enumerate(self.queue[:]):
                if node.priority <= element.priority:
                    self.queue.insert(i, node)

            self.queue.append(node)

    def pull(self):
        highest = self.queue.pop(len(self.queue) - 1)
        return highest


class Node:
    def __init__(self, x, y, priority):
        self.x = x
        self.y = y
        self.priority = priority

    def neighbours(self):
        coords = []
        for j in range(self.y-1, self.y+2):
            for i in range(self.x - 1, self.x + 2):
                if i >= 0 and j >= 0 and j < 20 and i < 20:
                    coords.append(Node(i, j, 0))

        return coords

    def is_same(self, comp):
        if self.x == comp.x and self.y == comp.y:
            return True

        return False

class AStar(Gtk.DrawingArea):
    def __init__(self, start, goal):
        super().__init__()
        self.board = [list(0 for i in range(20)) for j in range(20)]
        self.start = start
        self.goal = goal

        self.q = PriorityQueue()
        self.q.p_insert(self.start)
        self.visited = []
        self.path = {}

        self.g_score = {}
        self.g_score[self.start] = 0

        self.f_score = {}
        self.f_score[self.start] = self.h(self.start, self.goal)

        # Initialized upon self.run()
        self.draw_x = 0
        self.draw_y = 0

        # Drawing
        self.connect("draw", self.draw_board)
        GLib.timeout_add(10, self.update)

    def update(self):
        self.queue_draw()
        return True

    def h(self, start, goal):
        return sqrt((start.x - goal.x)**2 + (start.y - goal.y)**2)

    def construct_path(self, path, current):
        total = [current]
        while current in path.keys():
            current = path[current]
            total.insert(0, current)

        return total

    def draw_board(self, widget, ctx):
        for j in range(20):
            for i in range(20):
                ctx.set_source_rgb(0, 0, 0)
                rx = 80 + 35 * i
                ry = 80 + 35 * j

                ctx.rectangle(rx, ry, 35, 35)
                ctx.stroke()

                if self.board[j][i] == BoardObjects.WALL.value:
                    ctx.set_source_rgb(0.3, 0.3, 0.3)
                    ctx.rectangle(rx, ry, 35, 35)
                    ctx.fill()

        self.color_square(ctx)

    def color_square(self, ctx):
        for coord in self.visited[:]:
            rx = 80 + coord.x*35
            ry = 80 + coord.y*35
            ctx.set_source_rgb(0, 0, 0)
            ctx.rectangle(rx, ry, 35, 35)
            ctx.stroke()

            ctx.set_source_rgb(0, 0.7, 0.5)
            ctx.rectangle(rx, ry, 35, 35)
            ctx.fill()
            if self.board[coord.y][coord.x] == BoardObjects.VISITED.value:
                self.board[coord.y][coord.x] = BoardObjects.DRAWN.value
                return True

        for sqr in self.path:
            rx = 80 + sqr.x*35
            ry = 80 + sqr.y*35
            ctx.set_source_rgb(0, 0, 0)
            ctx.rectangle(rx, ry, 35, 35)
            ctx.stroke()

            ctx.set_source_rgb(1, 0.3, 0.4)
            ctx.rectangle(rx, ry, 35, 35)
            ctx.fill()



    def run(self):
        while self.q.queue:
            current = self.q.pull()
            if self.goal.is_same(current):
                self.path = self.construct_path(self.path, current)
                return self.path

            neighbours = current.neighbours()
            for neigh in neighbours[:]:
                if self.board[neigh.y][neigh.x] == BoardObjects.WALL.value:
                    continue

                self.path[neigh] = current
                self.board[neigh.y][neigh.x] = BoardObjects.VISITED.value
                self.g_score[neigh] = self.g_score[current] + 1
                self.f_score[neigh] = self.g_score[neigh] + abs(self.h(neigh, self.goal) - self.h(current, self.goal))

                neigh.priority = self.f_score[neigh]
                self.goal.priority = self.f_score[neigh]
                self.draw_x, self.draw_y = neigh.x, neigh.y
                if neigh not in self.visited:
                    self.q.p_insert(neigh)
                    self.visited.append(neigh)

    def reset(self):
        """Reset __init__ state"""
        self.board = [list(0 for i in range(20)) for j in range(20)]
        self.path = {}
        self.g_score = {}
        self.g_score[self.start] = 0
        self.f_score = {}
        self.f_score[self.start] = self.h(self.start, self.goal)
        self.visited = []
        self.q = PriorityQueue()
        self.q.p_insert(self.start)

