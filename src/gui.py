import gi
import math
from algdraw import AStar, Node
from enum import Enum
from pprint import pprint
gi.require_version("Gtk", "3.0")
from time import sleep
from math import sqrt
from gi.repository import GObject
from gi.repository import Gtk, Gdk, GLib

class MyWindow(Gtk.Window):
    allow_motion = False
    draw_x = None
    draw_y = None

    def __init__(self):
        Gtk.Window.__init__(self, title="Comparison")
        listbox = Gtk.ListBox()
        listbox.set_selection_mode(Gtk.SelectionMode.NONE)

        self.astar = Gtk.RadioButton.new_with_label_from_widget(None, "A*")
        djikstra = Gtk.RadioButton.new_with_label_from_widget(
            self.astar, "Djikstra")
        flood_fill = Gtk.RadioButton.new_with_label_from_widget(
            djikstra, "Flood fill")

        start_button = Gtk.Button(label="Start")
        reset_button = Gtk.Button(label="Reset")

        start_button.connect("clicked", self.on_button_clicked)
        reset_button.connect("clicked", self.reset_walls)

        listbox.add(self.astar)
        listbox.add(djikstra)
        listbox.add(flood_fill)
        listbox.add(start_button)
        listbox.add(reset_button)

        self.draw_field = self.get_drawing_field()
        box = Gtk.Box()
        box.pack_start(listbox, False, True, 0)
        box.pack_end(self.draw_field, True, True, 0)

        notebook = Gtk.Notebook()
        notebook.append_page(box, Gtk.Label(label="Map"))

        graph = Gtk.Box()

        notebook.append_page(graph, Gtk.Label(label="Graph"))
        self.add(notebook)

    def on_button_clicked(self, widget):
        self.draw_field.run()

    def get_drawing_field(self):
        drawing_field = AStar(Node(0, 0, 0), Node(19, 19, 0))
        drawing_field.connect("button-press-event", self.update_cell)
        drawing_field.connect("button-release-event", self.on_release)
        drawing_field.connect("motion-notify-event", self.on_motion)

        drawing_field.set_events(
            drawing_field.get_events() | Gdk.EventMask.BUTTON_PRESS_MASK)
        drawing_field.set_events(
            drawing_field.get_events() | Gdk.EventMask.POINTER_MOTION_MASK)
        drawing_field.set_events(
            drawing_field.get_events() | Gdk.EventMask.BUTTON_RELEASE_MASK)
        drawing_field.set_events(drawing_field.get_events() | Gdk.EventMask.EXPOSURE_MASK)
        return drawing_field

    def _get_coordinates(self, rx, ry):
        x = math.floor((rx - 80) / 35)
        y = math.floor((ry - 80) / 35)

        if 0 <= x < 20 and 0 <= y < 20:
            return x, y

        return None, None

    def update_cell(self, widget, event):
        x, y = self._get_coordinates(event.x, event.y)
        if x == None or y == None:
            return
        self.draw_field.board[y][x] = -1
        self.allow_motion = True
        self.draw_field.queue_draw()
        # print(event.x, event.y)

    def on_motion(self, widget, event):
        if self.allow_motion:
            x, y = self._get_coordinates(event.x, event.y)
            if x == None or y == None:
                return
            self.draw_field.board[y][x] = -1
            self.draw_field.queue_draw()

    def on_release(self, widget, event):
        self.allow_motion = False

    def reset_walls(self, widget):
        self.draw_field.reset()
        self.draw_field.queue_draw()

if __name__ == "__main__":
    win = MyWindow()
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()
