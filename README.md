<h1>Algorithms comparison tool</h1>

![](https://i.imgur.com/LLClUhZ.png)

<h1>Dependencies and install</h1>

<h2>Dependencies</h2>

<h3>System dependencies</h3>

- gtk3
- python-gobject

<h3>Python dependencies</h3>

- python >= 3.6
- setuptools for python

<h2>Installation</h2>

1. <h4>Installing dependencies</h4>

    Arch linux

    `sudo pacman -S python-gobject gtk3`

    other distros might come later

2. <h4>Clone repo</h4>

    `git clone https://gitlab.com/syomasa/alg-sim.git`

3. <h4>Install repo</h4>

    `cd alg-sim`

    `sudo pip install -e .`

<h2> Questions and answers </h2>

<h4>What does it do?</h4>

it allows you to visualise different kinds of algorithms

<h4>Why does it look so ugly</h4>

There is barely any kind of styling (a few colors here and there) otherwise there it is completly dependant on user gtk style.

<h2>To-Do</h2>

- Improve A* it gets stuck when wall shape is something similar to L
- other algorithms
- graphing
- refactoring
- timer
- fix visual bug where board isn't gettign cleared completly (use reset before trying to run algorithm again)
