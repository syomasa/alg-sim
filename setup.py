from setuptools import setup, find_packages

setup(
    name="algtest",
    author="syomasa",
    package_dir={"": "src"},
    python_requires=">=3.6",
    packages=find_packages(where="src"),
    url="https://gitlab.com/syomasa/alg-sim",
    license="MIT",
    entry_points={
        "console_scripts": [
            "algtest = main:run"
        ],
    }
)
